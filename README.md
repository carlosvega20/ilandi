cordova-plugin-ilandi
=====================

Is an ionic mobile building block that handles magnetic swipe reader(MSR) using Landi P960.

## Table Of Contents
- [Features](#features)
- [Prerequisites](#prerequisites)
- [Supported Platforms](#supported-platforms)

## Features
This block provides the following features:
	
- Card Reader : 3-track bidirectional Magnetic Stripe Reader (MSR)

## Prerequisites
Prerequisites for using ionic-landi block:

## Supported Platforms

- Android
